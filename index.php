<!DOCTYPE html>
<html>
<head>
    <title>Lesson 5 homework</title>
    <link rel="stylesheet" href="styles.css">
</head>

<body>

<h2 class="done">Задача 1:</h2>

<form action="" method="post">
    Введите имя: <br>
    <input
            type="text"
            name="name"
            placeholder="Имя"
            value="<?php if (isset($_REQUEST['name'])) echo htmlentities($_REQUEST['name']); ?>"
    >
    <br>
    Введите фамилию:<br>
    <input
            type="text"
            name="surname"
            placeholder="Фамилия"
            value="<?php if (isset($_REQUEST['surname'])) echo htmlentities($_REQUEST['surname']); ?>"
    > <br>
    Напишите автобиографию:<br>
    <textarea
            cols="33"
            rows="10"
            name="biography"
            placeholder="Биография"
    ><?php if (isset($_REQUEST['biography'])) echo htmlentities($_REQUEST['biography']); ?></textarea> <br>
    <input type="submit" value="Вывести результат"><br>

</form>
<br>
<h4>Результат:</h4>
<?php echo $_REQUEST['name'] . ' ' . $_REQUEST['surname']; ?><br>
<?php echo nl2br(strip_tags($_REQUEST['biography'])); ?><br>

<hr>

<h2 class="done">Задача 2:</h2>

<form action="" method="post">
    Введите день, месяц и год цифрами!<br>
    День:
    <input
            type="number"
            name="day"
            placeholder="День"
            size="5"
            min="1"
            max="31"
            value="<?php if (isset($_REQUEST['day'])) echo htmlentities($_REQUEST['day']); ?>"
    >
    Месяц:
    <input
            type="number"
            name="month"
            placeholder="Месяц"
            size="5"
            min="1"
            max="12"
            value="<?php if (isset($_REQUEST['month'])) echo htmlentities($_REQUEST['month']); ?>"
    >
    Год:
    <input
            type="number"
            name="year"
            placeholder="Год"
            size="5"
            value="<?php if (isset($_REQUEST['year'])) echo htmlentities($_REQUEST['year']); ?>"
    >
    <input type="submit" value="Вывести дату"><br>
</form>

<h4>Результат:</h4>
Дата:
<?php
if (isset($_REQUEST['day']) && (isset($_REQUEST['month'])) && (isset($_REQUEST['year']))) {
    if ($_REQUEST['day'] > 0 && $_REQUEST['day'] < 10) {
        $bday = '0' . $_REQUEST['day'];
    } else {
        $bday = $_REQUEST['day'];
    }
    if ($_REQUEST['month'] > 0 && $_REQUEST['month'] < 10) {
        $bmonth = '0' . $_REQUEST['month'];
    } else {
        $bmonth = $_REQUEST['month'];
    }
    echo $_REQUEST['year'] . '-' . $bmonth . '-' . $bday;
}
?>

<hr>

<h2 class="done">Задача 3:</h2>

<form action="" method="post">
    Введите 5 чисел в поля ниже:<br>
    <input
            type="number"
            name="1"
            placeholder="число 1"
            size="6"
            value="<?php if (isset($_POST['1'])) echo htmlentities($_POST['1']); ?>"
    >
    <input
            type="number"
            name="2"
            placeholder="число 2"
            size="6"
            value="<?php if (isset($_POST['2'])) echo htmlentities($_POST['2']); ?>"
    >
    <input
            type="number"
            name="3"
            placeholder="число 3"
            size="6"
            value="<?php if (isset($_POST['3'])) echo htmlentities($_POST['3']); ?>"
    >
    <input
            type="number"
            name="4"
            placeholder="число 4"
            size="6"
            value="<?php if (isset($_POST['4'])) echo htmlentities($_POST['4']); ?>"
    >
    <input
            type="number"
            name="5"
            placeholder="число 5"
            size="6"
            value="<?php if (isset($_POST['5'])) echo htmlentities($_POST['5']); ?>"
    ><br>
    <input type="submit" value="Вывести минимальное и максимальное">
</form>

<h4>Результат:</h4>

<?php
$max = max($_POST);
echo "Максимальное число: $max";
?><br>
<?php
$min = min($_POST);
echo "Минимальное число: $min";
?><br>

<hr>

<h2 class="done">Задача 4:</h2>

Введите данные для регистрации:
<form action="" method="post">
    Логин (должен содержать только буквы латинского алфавита и цифры, а также быть не больше 15 символов):<br>
    <input
            type="text"
            name="login"
            required
            maxlength="15"
            value="<?php if (isset($_POST['login'])) echo htmlentities($_POST['login']); ?>"
    >
    <?php
    if (preg_match('/^[A-Za-z0-9]+$/', $_POST['login']) == 1) {
        echo 'Условие выполнено. Логин подходит';
    } else
        echo 'Условие не выполнено, попробуйте другой логин';
    ?><br>

    Пароль:<br>
    <input
            type="password"
            name="pass"
            value="<?php if (isset($_POST['pass'])) echo htmlentities($_POST['pass']); ?>"
    ><br>
    Подтверждение пароля:<br>
    <input
            type="password"
            name="pass_confirm"
            value="<?php if (isset($_POST['pass_confirm'])) echo htmlentities($_POST['pass_confirm']); ?>"
    >
    <?php
    if ($_POST['pass'] === $_POST['pass_confirm']) {
        echo 'Пароли совпадают';
    } else {
        echo 'Пароли не совпадают';
    }
    ?><br><br>

    <input type="submit" value="Проверить данные">

</form>

<h4>Результат:</h4>
<?php
$login_length = strlen($_POST['login']);
$password_length = strlen($_POST['pass']);
if ( ($login_length > 3 && $login_length < 12) && ( $password_length > 5 && $password_length <9 ) ) {
    echo "Успешная регистрация";
} else {
    echo "Регистрация не удалась";
}
?>
<br><br>

<hr>

<h2 class="done">Задача 5:</h2>

<h4>Результат:</h4>
<?php
$page = $_GET['page'];
$article = $_GET['article'];
echo '$page = ' . $page . '; $article = ' . $article;
?>


<hr>

<h2 class="not_done">Задача 6:</h2>

<h4>Результат:</h4>
Задача не выполнена!
<hr>

<h2 class="done">Задача 7:</h2>
Напишите свой возраст:<br>
<form action="" method="post">
    <input
            type="number"
            name="age"
            min="10"
            max="80"
        <?php
        $age = $_POST['age'];
        ?>
            value="<?php echo $age; ?>"
    >
    <input type="submit" value="Записать">
</form>
<h4>Результат:</h4>
<?php echo 'Записанное значение: ' . $age; ?>

<hr>

<h2 class="done">Задача 8:</h2>
Выбери какие языки ты знаешь:
<form action="" method="post">
    <input type="checkbox" name="lang[]" value="Java">Java<br>
    <input type="checkbox" name="lang[]" value="Python">Python<br>
    <input type="checkbox" name="lang[]" value="PHP">PHP<br>
    <input type="checkbox" name="lang[]" value="Javascript">Javascript<br>
    <input type="checkbox" name="lang[]" value="Other">Other<br>
    <input type="submit" value="Вывести языки">
</form>

<h4>Результат:</h4>
<?php
echo 'Ты знаешь такие языки из нашего списка: ';
$lang = $_POST['lang'];
$n = count($lang);
if (empty($lang)) {
    echo 'ниодного, потому что ниодин из языков не выбран';
} else {
    $n = count($lang);
    for ($i = 0; $i <= $n; $i++) {
        echo $lang[$i] . PHP_EOL;
    }
}
?>

<hr>

<h2 class="done">Задача 9:</h2>
Выбери возраст:<br>
<form action="" method="post">
    <input type="radio" name="radio[]" value="менее 20 лет">менее 20 лет<br>
    <input type="radio" name="radio[]" value="20-25">20-25<br>
    <input type="radio" name="radio[]" value="26-30">26-30<br>
    <input type="radio" name="radio[]" value="более 30">более 30<br>
    <input type="submit" value="Вывести возраст">
</form>

<h4>Результат:</h4>
<?php
echo 'Ты выбрал возраст: ';
$age = $_POST['radio'];
if (empty($age)) {
    echo 'нет, не выбрал';
} else {
    echo $age[0];
}
?>

<hr>

<h2 class="done">Задача 10:</h2>
Выбери возраст:
<form action="" method="post">
    <select name="selector">
        <option value="дефисы какие-то, а не возраст. Выбери еще раз">--</option>
        <option value="менее 20 лет">менее 20 лет</option>
        <option value="20-25">20-25</option>
        <option value="26-30">26-30</option>
        <option value="более 30">более 30</option>
    </select>
    <input type="submit" name="submit" value="Записать значение и вывести в результат">
</form>

<h4>Результат:</h4>
<?php
if (isset($_POST['submit'])) {
    echo 'Ты выбрал: ' . $_POST['selector'];
}
?>

<hr>

<h2 class="not_done">Задача 11:</h2>

<h4>Результат:</h4>
Задача не выполнена!

<hr>

<h2 class="done">Задача 12:</h2>

<?php
$arr = [1,2,3,4,5];
?>

<h4>Результат:</h4>
<?php
foreach ($arr as $item) { ?>
    <p> <?php echo 'Елемент массива: ' . $item; ?></p><?php
}
?>

<hr>

<h2 class="done">Задача 13:</h2>
<?php
    $arr = [
        ['name'=>'Коля', 'age'=>30, 'salary'=>500],
        ['name'=>'Вася', 'age'=>31, 'salary'=>600],
        ['name'=>'Петя', 'age'=>32, 'salary'=>700],
    ];
print_r($arr);
?>

<h4>Результат:</h4>
    <table border="1">
        <?php foreach ($arr as $row) { ?>
            <tr>
                <td><?php echo implode('</td><td>', $row); ?></td>
            </tr>
        <?php } ?>
    </table>

<hr>

<h2 class="not_done">Задача 14:</h2>

<h4>Результат:</h4>
Задача не выполнена!

<hr>

<h2 class="not_done">Задача 15:</h2>

<h4>Результат:</h4>
Задача не выполнена!

<hr>

<h2 class="not_done">Задача 16:</h2>

<h4>Результат:</h4>
Задача не выполнена!

<hr>

</body>
</html>